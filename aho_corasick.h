
/*
Copyright (c) 2018 Giovanni Mascellani <gio@debian.org>

This file is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This file is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this file.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <map>
#include <memory>
#include <vector>
#include <list>
#include <ostream>
#include <utility>

#include <boost/type_traits/function_traits.hpp>

struct aho_corasick_params {
    bool discard_suffixes = true;
};

template< typename CharType, typename ValueType >
class aho_corasick;

template< typename CharType, typename ValueType >
class aho_corasick_node {
public:
    aho_corasick_node()
        : suffix(nullptr), dict_suffix(nullptr) {
    }

    aho_corasick_node *get_child(const CharType &x) const {
        auto it = this->children.find(x);
        if (it == this->children.end()) {
            return nullptr;
        } else {
            return it->second;
        }
    }

    aho_corasick_node *get_or_create_child(const CharType &x, aho_corasick< CharType, ValueType > &ac) {
        auto it = this->children.lower_bound(x);
        if (it != this->children.end() && it->first == x) {
            return it->second;
        } else {
            auto node = ac.alloc_node();
            this->children.insert(it, std::make_pair(x, node));
            return node;
        }
    }

    aho_corasick_node *get_suffix() const {
        return this->suffix;
    }

    void set_suffix(aho_corasick_node *suffix) {
        this->suffix = suffix;
    }

    aho_corasick_node *get_dict_suffix() const {
        return this->dict_suffix;
    }

    void set_dict_suffix(aho_corasick_node *dict_suffix) {
        this->dict_suffix = dict_suffix;
    }

    void put_dict_value(size_t size, const ValueType &x) {
        this->dict_values.push_back(std::make_pair(size, x));
    }

    bool is_terminal() const {
        return !this->dict_values.empty();
    }

    decltype(auto) get_dict_values_begin() const {
        return this->dict_values.begin();
    }

    decltype(auto) get_dict_values_end() const {
        return this->dict_values.end();
    }

    decltype(auto) get_children_begin() const {
        return this->children.begin();
    }

    decltype(auto) get_children_end() const {
        return this->children.end();
    }

private:
    std::map< CharType, aho_corasick_node* > children;
    aho_corasick_node *suffix;
    aho_corasick_node *dict_suffix;
    std::vector< std::pair< size_t, ValueType > > dict_values;
};

template< typename CorpusIter, typename ValueType >
class aho_corasick_results {
public:
    aho_corasick_results(CorpusIter first, CorpusIter last, aho_corasick< typename std::iterator_traits< CorpusIter >::value_type, ValueType > &ac)
        : first(first), current(first), last(last), ac(ac), node(ac.root_node), returning_node(nullptr) {
    }

    std::tuple< CorpusIter, CorpusIter, ValueType > operator()() {
        while (this->returning_node == nullptr) {
            if (this->current == this->last) {
                return { this->last, this-> last, ValueType{} };
            }
            while (true) {
                auto new_node = this->node->get_child(*this->current);
                if (new_node != nullptr) {
                    this->node = new_node;
                    break;
                }
                this->node = this->node->get_suffix();
                if (this->node == nullptr) {
                    this->node = ac.root_node;
                    break;
                }
            }
            ++this->current;
            if (this->node->is_terminal()) {
                this->returning_node = this->node;
            } else {
                this->returning_node = this->node->get_dict_suffix();
            }
            if (this->returning_node != nullptr) {
                this->returning_it = this->returning_node->get_dict_values_begin();
            }
        }
        auto ret = std::make_tuple(this->current - this->returning_it->first, this->current, this->returning_it->second);
        ++this->returning_it;
        if (this->returning_it == this->returning_node->get_dict_values_end()) {
            if (this->ac.params.discard_suffixes) {
                this->returning_node = nullptr;
            } else {
                this->returning_node = this->returning_node->get_dict_suffix();
            }
            if (this->returning_node != nullptr) {
                this->returning_it = this->returning_node->get_dict_values_begin();
            }
        }
        return ret;
    }

private:
    CorpusIter first;
    CorpusIter current;
    CorpusIter last;
    aho_corasick< typename std::iterator_traits< CorpusIter >::value_type, ValueType > &ac;
    aho_corasick_node< typename std::iterator_traits< CorpusIter >::value_type, ValueType > *node;
    aho_corasick_node< typename std::iterator_traits< CorpusIter >::value_type, ValueType > *returning_node;
    decltype(std::declval< aho_corasick_node< typename std::iterator_traits< CorpusIter >::value_type, ValueType > >().get_dict_values_begin()) returning_it;
};

template< typename CharType, typename ValueType >
class aho_corasick {
    friend class aho_corasick_node< CharType, ValueType >;
    template< typename, typename >
    friend class aho_corasick_results;

public:
    aho_corasick(const aho_corasick_params &params = {}) : params(params), finalized(false) {
        this->root_node = this->alloc_node();
    }
    template< typename PatIter >
    aho_corasick(PatIter first, PatIter last, ValueType &&value = {}, const aho_corasick_params &params = {})
        : aho_corasick(params) {
        this->add_pattern(first, last, std::forward< ValueType >(value));
    }
    ~aho_corasick() {}

    template< typename PatIter >
    void add_pattern(PatIter first, PatIter last, const ValueType &value = {}) {
        if (finalized) {
            throw std::logic_error("Calling add_pattern() on a finalized aho_corasick object");
        }
        auto node = this->root_node;
        size_t size = 0;
        for (; first != last; ++first) {
            node = node->get_or_create_child(*first, *this);
            ++size;
        }
        node->put_dict_value(size, value);
    }

    template< typename T >
    void add_pattern(const T &pat, const ValueType &value = {}) {
        this->add_pattern(std::begin(pat), std::end(pat), value);
    }

    void finalize() {
        if (this->finalized) {
            return;
        }
        this->finalized = true;
        std::list< std::tuple< aho_corasick_node< CharType, ValueType >*, aho_corasick_node< CharType, ValueType >*, CharType > > queue;
        queue.push_back(std::make_tuple(this->root_node, nullptr, CharType{}));
        while (!queue.empty()) {
            auto front = queue.front();
            auto node = std::get<0>(front);
            auto parent = std::get<1>(front);
            auto c = std::get<2>(front);
            queue.pop_front();
            for (auto it = node->get_children_begin(); it != node->get_children_end(); ++it) {
                queue.push_back(std::make_tuple(it->second, node, it->first));
            }
            if (parent != nullptr) {
                aho_corasick_node< CharType, ValueType > *suffix = nullptr;
                auto ref = parent->get_suffix();
                while (ref != nullptr) {
                    auto new_child = ref->get_child(c);
                    if (new_child != nullptr) {
                        suffix = new_child;
                        break;
                    }
                    ref = ref->get_suffix();
                }
                if (suffix == nullptr) {
                    suffix = this->root_node;
                }
                node->set_suffix(suffix);
                if (suffix->is_terminal()) {
                    node->set_dict_suffix(suffix);
                } else {
                    node->set_dict_suffix(suffix->get_dict_suffix());
                }
            }
        }
    }

    void dump(std::ostream &str) {
        str << "Aho-Corasick data has " << this->nodes_alloc.size() << " nodes" << std::endl;
        for (auto &node : this->nodes_alloc) {
            str << "Node " << node.get() << ": is_terminal -> " << node->is_terminal() << "; suffix -> " << node->get_suffix() << "; dict_suffix -> " << node->get_dict_suffix() << std::endl;
            for (auto it = node->get_children_begin(); it != node->get_children_end(); ++it) {
                str << "  Children " << it->second << " labelled " << it->first << std::endl;
            }
            for (auto it = node->get_dict_values_begin(); it != node->get_dict_values_end(); ++it) {
                str << "  Termination value of size " << it->first << ": " << it->second << std::endl;
            }
        }
    }

    template< typename CorpusIter >
    decltype(auto) get_results(CorpusIter first, CorpusIter last) {
        if (!this->finalized) {
            this->finalize();
        }
        return aho_corasick_results< CorpusIter, ValueType >(first, last, *this);
    }

    template< typename T >
    decltype(auto) get_results(const T &corpus) {
        return this->get_results(std::begin(corpus), std::end(corpus));
    }

    template< typename CorpusIter >
    decltype(auto) operator()(CorpusIter first, CorpusIter last) {
        return this->get_results(first, last)();
    }

    template< typename T >
    decltype(auto) operator()(const T &corpus) {
        return this->operator()(std::begin(corpus), std::end(corpus));
    }

private:
    decltype(auto) alloc_node() {
        this->nodes_alloc.push_back(std::make_unique< aho_corasick_node< CharType, ValueType > >());
        return this->nodes_alloc.back().get();
    }

    const aho_corasick_params params;
    bool finalized;
    std::vector< std::unique_ptr< aho_corasick_node< CharType, ValueType > > > nodes_alloc;
    aho_corasick_node< CharType, ValueType > *root_node;
};
