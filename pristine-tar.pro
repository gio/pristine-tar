TEMPLATE = app
CONFIG += console c++14
CONFIG -= app_bundle
CONFIG -= qt

QMAKE_LIBS += -lboost_filesystem -lboost_system -lboost_iostreams

macx {
    QMAKE_CXXFLAGS += -I/usr/local/include
    QMAKE_LIBS += -L/usr/local/lib
}

SOURCES += \
    main.cpp

HEADERS += \
    xxhash.h \
    aho_corasick.h \
    rabin_karp.h \
    picosha2.h

DISTFILES += \
    README
