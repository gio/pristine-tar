
/*
Copyright (c) 2018 Giovanni Mascellani <gio@debian.org>

This file is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This file is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this file.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <map>
#include <iterator>

#include <boost/endian/conversion.hpp>
#include <boost/filesystem.hpp>
#include <boost/iostreams/device/mapped_file.hpp>
#include <boost/algorithm/searching/knuth_morris_pratt.hpp>
#include <boost/algorithm/searching/boyer_moore_horspool.hpp>
#include <boost/algorithm/searching/boyer_moore.hpp>

#include "picosha2.h"
#include "aho_corasick.h"
#include "rabin_karp.h"

#define XXH_INLINE_ALL
#include "xxhash.h"

const uint64_t MIN_FILE_SIZE = 16;
const uint64_t COPY_BUF_SIZE = 4096;
const uint64_t BLOCK_SIZE = 512;

struct XXHash {
    typedef uint64_t HashValue;
    template< typename IterType >
    HashValue compute_block_hash(IterType begin, IterType end) {
        return XXH64(&*begin, static_cast< size_t >(end - begin), 0);
    }
};

struct SHA2Hash {
    typedef std::array< char, picosha2::k_digest_size > HashValue;
    template< typename IterType >
    HashValue compute_block_hash(IterType begin, IterType end) {
        HashValue ret;
        picosha2::hash256(begin, end, ret.begin(), ret.end());
        return ret;
    }
};

template< typename HashType >
decltype(auto) compute_file_hash(boost::filesystem::path p) {
    HashType hash;
    std::vector< typename HashType::HashValue > ret;
    std::vector< char > block(BLOCK_SIZE, 0);
    boost::filesystem::ifstream fin(p);
    while (fin) {
        fin.read(block.data(), BLOCK_SIZE);
        auto size = fin.gcount();
        if (size == 0) {
            break;
        }
        std::fill(block.begin() + size, block.end(), 0);
        ret.push_back(hash.compute_block_hash(block.begin(), block.end()));
    }
    //std::cerr << " Hashes for file " << p << " have length " << ret.size() << std::endl;
    return ret;
}

struct MMapRepr {
    typedef boost::iostreams::mapped_file_source ContainerType;
    typedef ContainerType::char_type ElemType;
    ContainerType get_representation(boost::filesystem::path p) {
        return boost::iostreams::mapped_file_source(p);
    }
    size_t get_multiplier() const { return 1; }
};

template< typename HashType >
struct HashRepr {
    typedef typename HashType::HashValue ElemType;
    typedef std::vector< ElemType > ContainerType;
    ContainerType get_representation(boost::filesystem::path p) {
        return compute_file_hash< HashType >(p);
    }
    size_t get_multiplier() const { return BLOCK_SIZE; }
};

void scan_dirs(boost::filesystem::path p, std::vector< std::tuple< uintmax_t, boost::filesystem::path > > &files) {
    if (is_regular_file(p)) {
        if (file_size(p) >= MIN_FILE_SIZE) {
            files.push_back(std::make_tuple(file_size(p), p));
        }
    } else if (is_directory(p)) {
        for (auto &child : boost::filesystem::directory_iterator(p)) {
            scan_dirs(child, files);
        }
    }
}

template< typename Repr >
void find_intervals(const typename Repr::ContainerType &tarball_repr,
                    std::map< std::pair< uintmax_t, uintmax_t >, boost::filesystem::path > &intervals,
                    const boost::filesystem::path file) {
    Repr repr;
    auto file_repr = repr.get_representation(file);
    auto size = file_size(file);
    boost::algorithm::boyer_moore< decltype(file_repr.begin()) > searcher(file_repr.begin(), file_repr.end());
    auto ptr = tarball_repr.begin();
    while (true) {
        auto res = searcher(ptr, tarball_repr.end());
        if (res.first == tarball_repr.end()) {
            break;
        } else {
            auto begin = repr.get_multiplier() * static_cast< uintmax_t >(res.first - tarball_repr.begin());
            auto end = begin + size;
            auto it = intervals.lower_bound(std::make_pair(begin, end));
            if (it == intervals.end() || end <= it->first.first) {
                if (it == intervals.begin() || (--it)->first.second <= begin) {
                    intervals.insert(std::make_pair(std::make_pair(begin, end), relative(file)));
                }
            }
            ptr = res.second;
        }
    }
}

template< typename Repr >
void add_pattern(aho_corasick< typename Repr::ElemType, boost::filesystem::path > &ac,
                 boost::filesystem::path file) {
    Repr repr;
    auto file_repr = repr.get_representation(file);
    ac.add_pattern(file_repr, file);
}

template< typename T >
void write_be_number(std::ostream &str, T number) {
    boost::endian::native_to_big_inplace(number);
    str.write(reinterpret_cast< char* >(&number), sizeof(number));
}

template< typename T >
T read_be_number(std::istream &str) {
    T number;
    str.read(reinterpret_cast< char* >(&number), sizeof(number));
    boost::endian::big_to_native_inplace(number);
    return number;
}

void stream_copy(std::istream &istr, std::ostream &ostr, std::uint64_t n) {
    std::vector< char > buf(COPY_BUF_SIZE);
    while (n > 0) {
        std::uint64_t len = std::min(n, COPY_BUF_SIZE);
        istr.read(buf.data(), static_cast< std::streamsize >(len));
        ostr.write(buf.data(), static_cast< std::streamsize >(len));
        n -= len;
    }
}

template< typename Repr = HashRepr< XXHash > >
void gendelta(boost::filesystem::path tarball, boost::filesystem::path delta) {
    std::vector< std::tuple< uintmax_t, boost::filesystem::path > > files;
    scan_dirs(".", files);
    sort(files.rbegin(), files.rend());
    std::map< std::pair< uintmax_t, uintmax_t >, boost::filesystem::path > intervals;
    Repr repr;
    auto tarball_repr = repr.get_representation(tarball);
    boost::iostreams::mapped_file_source tarball_data(tarball);
    aho_corasick< typename Repr::ElemType, boost::filesystem::path > ac;
    for (const auto &f : files) {
        //find_intervals< Repr >(tarball_repr, intervals, std::get<1>(f));
        add_pattern< Repr >(ac, std::get<1>(f));
    }
    ac.finalize();
    //ac.dump(std::cerr);

    auto result = ac.get_results(tarball_repr);
    while (true) {
        auto tmp = result();
        auto begin = std::get<0>(tmp);
        auto path = std::get<2>(tmp);
        if (begin == tarball_repr.end()) {
            break;
        }
        //std::cerr << "Found result " << path << " from " << begin - tarball_repr.begin() << " to " << end - tarball_repr.begin() << std::endl;
        auto adj_begin = repr.get_multiplier() * static_cast< uintmax_t >(begin - tarball_repr.begin());
        auto adj_end = adj_begin + file_size(path);
        /* TODO: here we use a simple greedy algorithm to select intervals,
         * which if ok is intervals will rarely overlap;
         * however, if intervals overlap a smaller interval could prevent
         * the insertion of a much larger one, which would be a waste.
         * It would be better, when inserting a conflicting interval, to
         * drop or truncate smaller interval which are in conflict. This
         * should be doable without putting additional complexity. */
        auto it = intervals.lower_bound(std::make_pair(adj_begin, adj_end));
        if (it == intervals.end() || adj_end <= it->first.first) {
            if (it == intervals.begin() || (--it)->first.second <= adj_begin) {
                intervals.insert(std::make_pair(std::make_pair(adj_begin, adj_end), relative(path)));
            }
        }
    }

    /*std::cerr << "The following files were found:" << std::endl;
    for (const auto &inter : intervals) {
        std::cerr << "  From " << inter.first.first << " to " << inter.first.second << " in file " << inter.second.string() << std::endl;
    }*/

    // Add a fictional empty interval at the end
    intervals.insert(std::make_pair(std::make_pair(tarball_data.size(), tarball_data.size()), ""));

    boost::filesystem::ofstream fdelta(delta);
    fdelta.exceptions(std::ios_base::failbit);
    uintmax_t pos = 0;
    for (const auto &inter : intervals) {
        auto begin = inter.first.first;
        auto end = inter.first.second;
        auto file = inter.second;
        assert(pos <= begin);
        if (pos < begin) {
            write_be_number< uint8_t >(fdelta, 0);
            write_be_number< uint64_t >(fdelta, begin - pos);
            fdelta.write(tarball_data.data() + pos, static_cast< std::streamsize >(begin - pos));
            pos = begin;
        }
        assert(begin <= end);
        if (begin < end) {
            write_be_number< uint8_t >(fdelta, 1);
            write_be_number< uint64_t >(fdelta, end - begin);
            std::string file_str = file.string();
            fdelta.write(file_str.c_str(), static_cast< std::streamsize >(file_str.size() + 1));
            pos = end;
        }
    }
}

void gentar(boost::filesystem::path delta, boost::filesystem::path tarball) {
    boost::filesystem::ifstream fdelta(delta);
    boost::filesystem::ofstream ftarball(tarball);
    fdelta.exceptions(std::ios_base::failbit);
    ftarball.exceptions(std::ios_base::failbit);
    while (true) {
        uint8_t type;
        try {
            type = read_be_number< uint8_t >(fdelta);
        } catch (std::ios_base::failure &e) {
            (void) e;
            break;
        }
        if (type == 0) {
            auto size = read_be_number< uint64_t >(fdelta);
            stream_copy(fdelta, ftarball, size);
        } else if (type == 1) {
            auto size = read_be_number< uint64_t >(fdelta);
            std::string file_str;
            std::getline(fdelta, file_str, '\0');
            std::ifstream fin(file_str);
            stream_copy(fin, ftarball, size);
        } else {
            assert(!"Unknown record type");
        }
    }
}

void test_aho_corasick() {
    aho_corasick< char, int > ac;
    ac.add_pattern< std::string >("a", 0);
    ac.add_pattern< std::string >("ab", 1);
    ac.add_pattern< std::string >("bab", 2);
    ac.add_pattern< std::string >("bc", 3);
    ac.add_pattern< std::string >("bca", 4);
    ac.add_pattern< std::string >("c", 5);
    ac.add_pattern< std::string >("caa", 6);
    ac.add_pattern< std::string >("caa", 7);
    ac.finalize();
    //ac.dump(std::cerr);
    std::string corpus = "abccabda";
    auto res = ac.get_results(corpus);
    while (true) {
        auto tmp = res();
        auto begin = std::get<0>(tmp);
        auto end = std::get<1>(tmp);
        auto value = std::get<2>(tmp);
        if (begin == corpus.end()) {
            break;
        }
        std::cout << "Found result " << value << " from " << begin - corpus.begin() << " to " << end - corpus.begin() << std::endl;
    }
}

int main2(int, char*[]) {
    test_aho_corasick();
    return 0;
}

int main(int argc, char *argv[]) {
    if (argc < 2) {
        std::cerr << "Please specify an action" << std::endl;
        return 1;
    }
    auto action = argv[1];
    if (strcmp(action, "gendelta") == 0) {
        if (argc < 4) {
            std::cerr << "Please specify tarball and delta" << std::endl;
            return 1;
        }
        gendelta(argv[2], argv[3]);
    } else if (strcmp(action, "gentar") == 0) {
        if (argc < 4) {
            std::cerr << "Please specify delta and tarball" << std::endl;
            return 1;
        }
        gentar(argv[2], argv[3]);
    } else {
        std::cerr << "Please specify a valid action (gendelta or gentar)" << std::endl;
        return 1;
    }

    return 0;
}
