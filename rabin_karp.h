
/*
Copyright (c) 2018 Giovanni Mascellani <gio@debian.org>

This file is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This file is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this file.  If not, see <http://www.gnu.org/licenses/>.
*/

// This implementation is not finished and it will not work!

#pragma once

template< typename HashType, typename Iter >
class rabin_karp_hash {
public:
    rabin_karp_hash(HashType mult, Iter first, Iter last)
        : mult(mult), val(0), mult_pow(1) {
        size_t n = 0;
        for (; first < last; ++first) {
            this->val *= this->mult;
            this->val += static_cast< HashType >(*first);
            n++;
        }
        n--;
        for (size_t i = 0; i < n; i++) {
            this->mult_pow += this->mult;
        }
    }

    void roll(typename std::iterator_traits< Iter >::value_type in, typename std::iterator_traits< Iter >::value_type out) {
        this->val -= this->mult_pow * out;
        this->val *= this->mult;
        this->val += in;
    }

    HashType get_value() const {
        return this->val;
    }

private:
    HashType mult;
    HashType val;
    HashType mult_pow;
};

template< typename PatIter, typename ValueType >
class rabin_karp {
public:
    rabin_karp() {}
    rabin_karp(PatIter first, PatIter last, ValueType &&value = {})
        : rabin_karp() {
        this->add_pattern(first, last, std::forward< ValueType >(value));
    }
    ~rabin_karp() {}
    void add_pattern(PatIter first, PatIter last, ValueType &&value = {}) {
        this->hashes.insert(std::make_pair(rabin_karp_hash< uint64_t, PatIter >(6364136223846793005, first, last).get_value(), std::forward< ValueType >(value)));
    }

    template< typename CorpusIter >
    std::pair< CorpusIter, CorpusIter > operator()(CorpusIter first, CorpusIter last) {
        return { last, last };
    }
private:
    std::multimap< uint64_t, ValueType > hashes;
};
